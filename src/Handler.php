<?php
declare(strict_types=1);

namespace think\admin;

use Throwable;
use think\admin\exception\HttpResponseException;
use think\admin\exception\SuccessException;
use think\admin\exception\ErrorException;
use Tinywan\ExceptionHandler\Handler as ExceptionHandler;
use Webman\Http\Response;

class Handler extends ExceptionHandler
{
    /**
     * 响应结果数据.
     */
    protected $data = [];

    /**
     * Business code.
     * @var int
     */
    public $code = 0;

    /**
     * Business message.
     * @var string
     */
    public $message = '';

    /**
     * Business messageType.
     * @var string
     */
    public $messageType = 'info';

    /**
     * @inheritDoc
     */
    protected function solveExtraException(Throwable $e): void
    {
        if ($e instanceof HttpResponseException || $e instanceof SuccessException || $e instanceof ErrorException) {
            $this->errorMessage = "Http Response";
            $this->errorCode = 200;
            if (isset($e->data)) $this->data = $e->data;
            if (isset($e->code)) $this->code = $e->code;
            if (isset($e->message)) $this->message = $e->message;
            if (isset($e->messageType)) $this->messageType = $e->messageType;
            return;
        }

        parent::solveExtraException($e);
    }

    /**
     * 调试模式：错误处理器会显示异常以及详细的函数调用栈和源代码行数来帮助调试，将返回详细的异常信息。
     * @param Throwable $e
     * @return void
     */
    protected function addDebugInfoToResponse(Throwable $e): void
    {
        if (config('app.debug', false)) {
            $this->responseData['error'] = $this->error;
            $this->responseData['error_message'] = $this->errorMessage;
            $this->responseData['file'] = $e->getFile();
            $this->responseData['line'] = $e->getLine();
        }
    }

    /**
     * @inheritDoc
     */
    protected function buildResponse(): Response
    {
        $responseBody = [
            'code' => $this->code,
            $this->messageType => $this->message,
            'data' => $this->data
        ];
        if (config('app.debug')) $responseBody = array_merge($responseBody, ['responseData' => $this->responseData]);

        $header = array_merge(['Content-Type' => 'application/json;charset=utf-8'], $this->header);
        return new Response($this->statusCode, $header, json_encode($responseBody, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
    }
}