<?php
declare (strict_types=1);

namespace think\admin;

use think\facade\Cache;
use support\Request;
use support\Response;

/**
 * 模块服务
 * @class Library
 * @package think\admin
 */
class Library
{
    /**
     * 设置数据缓存
     * @param string $name 缓存名称
     * @param mixed $value 缓存内容
     * @return boolean
     */
    public static function setCache(string $name, $value, $expire = 0)
    {
        return Cache::set($name, $value, $expire);
    }

    /**
     * 获取缓存数据
     * @param string $name 缓存名称
     * @return integer|string|array
     */
    public static function getCache(string $name, $default = '')
    {
        return Cache::get($name, $default);
    }

    /**
     * 删除缓存数据
     * @param string $name 缓存名称
     * @return boolean
     */
    public static function deleteCache(?string $name = null)
    {
        return !$name ? Cache::clear() : Cache::delete($name);
    }

    /**
     * 设置数据session
     * @param string $name session名称
     * @param mixed $value session内容
     * @return boolean
     */
    public static function setSession(string $name, $value)
    {
        return session([$name => $value]);
    }

    /**
     * 获取session数据
     * @param string $name session名称
     * @return integer|string|array
     */
    public static function getSession(string $name, $default = '')
    {
        return session($name, $default);
    }

    /**
     * 删除session数据
     * @param string $name session名称
     * @return boolean
     */
    public static function deleteSession(?string $name = null)
    {
        return !$name ? session()->flush() : session()->forget($name);
    }
}