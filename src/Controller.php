<?php
declare (strict_types=1);

namespace think\admin;

use stdClass;
use think\admin\helper\DeleteHelper;
use think\admin\helper\FormHelper;
use think\admin\helper\PageHelper;
use think\admin\helper\QueryHelper;
use think\admin\helper\SaveHelper;
use think\admin\helper\ValidateHelper;
use think\admin\service\NodeService;
use think\admin\service\QueueService;
use think\db\BaseQuery;
use think\Model;
use support\Request;
use support\Response;
use think\admin\exception\HttpResponseException;

/**
 * 标准控制器基类
 * @class Controller
 * @package think\admin
 */
class Controller extends stdClass
{
    /**
     * 请求GET参数
     * @var array
     */
    public $get = [];

    /**
     * 当前功能节点
     * @var string
     */
    public $node;

    /**
     * 请求参数对象
     * @var Request
     */
    public $request;

    /**
     * Constructor.
     * @param App $app
     */
    public function __construct()
    {
        [$this->request, $this->get, $this->node] = [request(), request()->get(), NodeService::getCurrent()];
        $this->initialize();
    }

    /**
     * 控制器初始化
     */
    protected function initialize()
    {
    }

    /**
     * 返回格式化json
     * @param mixed $info 消息内容
     * @param mixed $data 返回数据
     * @param mixed $code 返回代码
     */
    public function json($info = 'ok', $data = '{-null-}', $code = 1)
    {
        if ($data === '{-null-}') $data = new stdClass();
        throw new HttpResponseException(is_string($info) ? lang($info) : $info, $data, $code, 'info');
    }

    /**
     * 返回失败的内容
     * @param mixed $info 消息内容
     * @param mixed $data 返回数据
     * @param mixed $code 返回代码
     */
    public function error($info, $data = '{-null-}', $code = 0)
    {
        $this->json($info, $data, $code);
    }

    /**
     * 返回成功的内容
     * @param mixed $info 消息内容
     * @param mixed $data 返回数据
     * @param mixed $code 返回代码
     */
    public function success($info, $data = '{-null-}', $code = 1)
    {
        $this->json($info, $data, $code);
    }

    /**
     * URL重定向
     * @param string $url 跳转链接
     * @param integer $code 跳转代码
     */
    public function redirect(string $url, int $code = 302)
    {
        return new Response($code, ['Location' => $url]);
    }

    /**
     * 返回视图内容
     * @param string $tpl 模板名称
     * @param array $vars 模板变量
     * @param null|string $app 授权节点
     */
    public function fetch(string $tpl = '', array $vars = [], ?string $app = null)
    {
        $request = request(); $this->get = $request->get(); $tplArr = explode("/", $tpl);
        $realController = strtolower(uncamelize(getRealController(), '-')); // webman兼容视图文件目录 驼峰式转'-'
        if (empty($tpl)) {
            [$controller, $action] = [$realController, $request->action];
            $tpl = $controller . '/' . $action;
        } else if (count($tplArr) == 1) {
            $tpl = $realController . '/' . $tpl;
        } else if (count($tplArr) >= 3) {
            $app = $tplArr[0]; unset($tplArr[0]); $tpl = implode("/", $tplArr);
        }
        foreach ($this as $name => $value) $vars[$name] = $value; // 只可直接fetch, Form助手获取不到
        foreach ($request->properties as $name => $value) $vars[$name] = $value; // 兼容快捷助手
        return view($tpl, $vars, $app);
    }

    /**
     * 模板变量赋值
     * @param mixed $name 要显示的模板变量
     * @param mixed $value 变量的值
     * @return $this
     */
    public function assign($name, $value = ''): Controller
    {
        if (is_string($name)) {
            $this->$name = $value;
        } elseif (is_array($name)) {
            foreach ($name as $k => $v) {
                if (is_string($k)) $this->$k = $v;
            }
        }
        return $this;
    }

    /**
     * 数据回调处理机制
     * @param string $name 回调方法名称
     * @param mixed $one 回调引用参数1
     * @param mixed $two 回调引用参数2
     * @param mixed $thr 回调引用参数3
     * @return boolean
     */
    public function callback(string $name, &$one = [], &$two = [], &$thr = []): bool
    {
        $request = request(); $controller = $request->controller; $controller = new $controller();
        if (is_callable($name)) return call_user_func($name, $controller, $one, $two, $thr);
        foreach (["_" . $request->action . "{$name}", $name] as $method) {
            if (method_exists($controller, $method) && false === $controller->$method($one, $two, $thr)) {
                return false;
            }
        }
        return true;
    }

    /**
     * 快捷查询逻辑器
     * @param BaseQuery|Model|string $dbQuery
     * @param array|string|null $input
     * @return QueryHelper
     * @throws \think\db\exception\DbException
     */
    protected function _query($dbQuery, $input = null): QueryHelper
    {
        return QueryHelper::instance()->init($dbQuery, $input);
    }

    /**
     * 快捷分页逻辑器
     * @param BaseQuery|Model|string $dbQuery
     * @param boolean|integer $page 是否分页或指定分页
     * @param boolean $display 是否渲染模板
     * @param boolean|integer $total 集合分页记录数
     * @param integer $limit 集合每页记录数
     * @param string $template 模板文件名称
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    protected function _page($dbQuery, $page = true, bool $display = true, $total = false, int $limit = 0, string $template = '')
    {
        return PageHelper::instance()->init($dbQuery, $page, $display, $total, $limit, $template);
    }

    /**
     * 快捷表单逻辑器
     * @param BaseQuery|Model|string $dbQuery
     * @param string $template 模板名称
     * @param string $field 指定数据主键
     * @param mixed $where 额外更新条件
     * @param array $data 表单扩展数据
     * @return array|boolean
     * @throws \think\admin\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    protected function _form($dbQuery, string $template = '', string $field = '', $where = [], array $data = [])
    {
        return FormHelper::instance()->init($dbQuery, $template, $field, $where, $data);
    }

    /**
     * 快捷输入并验证（ 支持 规则 # 别名 ）
     * @param array $rules 验证规则（ 验证信息数组 ）
     * @param string|array $type 输入方式 ( post. 或 get. )
     * @param callable|null $callable 异常处理操作
     * @return array
     */
    protected function _vali(array $rules, $type = '', ?callable $callable = null)
    {
        return ValidateHelper::instance()->init($rules, $type, $callable);
    }

    /**
     * 快捷更新逻辑器
     * @param BaseQuery|Model|string $dbQuery
     * @param array $data 表单扩展数据
     * @param string $field 数据对象主键
     * @param mixed $where 额外更新条件
     * @return boolean
     * @throws \think\db\exception\DbException
     */
    protected function _save($dbQuery, array $data = [], string $field = '', $where = []): bool
    {
        return SaveHelper::instance()->init($dbQuery, $data, $field, $where);
    }

    /**
     * 快捷删除逻辑器
     * @param BaseQuery|Model|string $dbQuery
     * @param string $field 数据对象主键
     * @param mixed $where 额外更新条件
     * @return boolean
     * @throws \think\db\exception\DbException
     */
    protected function _delete($dbQuery, string $field = '', $where = []): bool
    {
        return DeleteHelper::instance()->init($dbQuery, $field, $where);
    }

    /**
     * 创建异步任务并返回任务编号
     * @param string $title 任务名称
     * @param string $command 执行内容
     * @param integer $later 延时执行时间
     * @param array $data 任务附加数据
     * @param integer $rscript 任务类型(0单例,1多例)
     * @param integer $loops 循环等待时间
     */
    protected function _queue(string $title, string $command, int $later = 0, array $data = [], int $rscript = 0, int $loops = 0)
    {
        try {
            $queue = QueueService::register($title, $command, $later, $data, $rscript, $loops);
            $this->success('创建任务成功！', $queue->code);
        } catch (Exception $exception) {
            $code = $exception->getData();
            if (is_string($code) && stripos($code, 'Q') === 0) {
                $this->success('任务已经存在，无需再次创建！', $code);
            } else {
                $this->error($exception->getMessage());
            }
        } catch (HttpResponseException $exception) {
            throw $exception;
        } catch (\Exception $exception) {
            trace_file($exception);
            $this->error(lang('创建任务失败，%s', [$exception->getMessage()]));
        }
    }
}
