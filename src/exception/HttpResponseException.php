<?php
declare(strict_types=1);

namespace think\admin\exception;

class HttpResponseException extends BaseException
{
    /**
     * @var int
     */
    public int $statusCode = 200;

    /**
     * @var string
     */
    public string $errorMessage = 'Http Response';
}
