<?php
/**
 * @desc BaseException
 * @author Tinywan(ShaoBo Wan)
 * @email 756684177@qq.com
 * @date 2022/3/6 14:14
 */

declare(strict_types=1);

namespace think\admin\Exception;

class BaseException extends \Exception
{
    /**
     * HTTP Response Status Code.
     */
    public int $statusCode = 200;

    /**
     * HTTP Response Header.
     */
    public array $header = [];

    /**
     * Business Error code.
     *
     * @var int|mixed
     */
    public int $errorCode = 0;

    /**
     * Business Error message.
     * @var string
     */
    public string $errorMessage = 'The requested resource is not available or not exists';

    /**
     * Business data.
     * @var array|mixed
     */
    public $data = [];

    /**
     * Detail Log Error message.
     * @var string
     */
    public string $error = '';

    /**
     * Business code.
     * @var int
     */
    public $code = 0;

    /**
     * Business message.
     * @var string
     */
    public $message = '';

    /**
     * Business messageType.
     * @var string
     */
    public $messageType = 'info';

    /**
     * BaseException constructor.
     * @param string $errorMessage
     * @param array $params
     * @param string $error
     */
    public function __construct(string $errorMessage = '', $data = [], int $code = 0, string $msgType = 'info', array $header = [], string $error = '')
    {
        parent::__construct($errorMessage, $this->statusCode);
        if (!empty($errorMessage))  $this->errorMessage = $errorMessage;
        if (!empty($error)) $this->error = $error;
        if (!empty($header)) $this->header = $header;
        $this->code = $code;
        $this->data = $data;
        $this->message = $errorMessage;
        if (!empty($msgType)) $this->messageType = $msgType;
    }
}
