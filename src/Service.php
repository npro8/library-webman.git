<?php
declare (strict_types=1);

namespace think\admin;

use think\Container;

/**
 * 自定义服务基类
 * @class Service
 * @package think\admin
 */
abstract class Service
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->initialize();
    }

    /**
     * 初始化服务
     */
    protected function initialize()
    {
    }

    /**
     * 静态实例对象
     * @param array $var 实例参数
     * @param boolean $new 创建新实例
     * @return static|mixed
     */
    public static function instance(array $var = [], bool $new = false)
    {
        return Container::getInstance()->make(static::class, $var, $new);
    }
}