<?php
namespace think\admin;

/**
 * Class Request
 * @package think\admin
 */
class Request extends \support\Request
{
    /**
     * isGet
     * @return boolean
     */
    public function isGet(): bool
    {
        return $this->method() == 'GET';
    }

    /**
     * isPost
     * @return boolean
     */
    public function isPost(): bool
    {
        return $this->method() == 'POST';
    }
}