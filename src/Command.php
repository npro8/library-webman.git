<?php
declare (strict_types=1);

namespace think\admin;

use think\admin\service\ProcessService;
use think\admin\service\QueueService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * 自定义指令基类
 * @class Command
 * @package think\admin
 */
abstract class Command extends \Symfony\Component\Console\Command\Command
{
    /**
     * 任务控制服务
     * @var QueueService
     */
    protected $queue;

    /**
     * 进程控制服务
     * @var ProcessService
     */
    protected $process;

    /**
     * input
     * @var InputInterface
     */
    protected $input;

    /**
     * output
     * @var OutputInterface
     */
    protected $output;

    /**
     * 初始化指令变量
     * @param \Symfony\Component\Console\Input\InputInterface
     * @param \Symfony\Component\Console\Output\OutputInterface
     * @return $this
     * @throws \think\admin\Exception
     */
    protected function initialize(InputInterface $input, OutputInterface $output): Command
    {
        $this->queue = QueueService::instance();
        $this->process = ProcessService::instance();
        [$this->input, $this->output] = [$input, $output];
        if (defined('WorkQueueCode') && $this->queue->code !== WorkQueueCode) {
            $this->queue->initialize(WorkQueueCode);
        }
        return $this;
    }

    /**
     * 设置失败消息并结束进程
     * @param string $message 消息内容
     * @throws \think\admin\Exception
     */
    protected function setQueueError(string $message)
    {
        if (defined('WorkQueueCode')) {
            return $this->queue->error($message);
        } else {
            return $this->process->message($message);
            exit(0);
        }
    }

    /**
     * 设置成功消息并结束进程
     * @param string $message 消息内容
     * @throws \think\admin\Exception
     */
    protected function setQueueSuccess(string $message)
    {
        if (defined('WorkQueueCode')) {
            return $this->queue->success($message);
        } else {
            return $this->process->message($message);
            exit(0);
        }
    }

    /**
     * 设置进度消息并继续执行
     * @param null|string $message 进度消息
     * @param null|string $progress 进度数值
     * @param integer $backline 回退行数
     * @return static
     * @throws \think\admin\Exception
     */
    protected function setQueueProgress(?string $message = null, ?string $progress = null, int $backline = 0): Command
    {
        if (defined('WorkQueueCode')) {
            $this->queue->progress(2, $message, $progress, $backline);
        } elseif (is_string($message)) {
            $this->process->message($message, $backline);
        }
        return $this;
    }

    /**
     * 更新任务进度
     * @param integer $total 记录总和
     * @param integer $count 当前记录
     * @param string $message 文字描述
     * @param integer $backline 回退行数
     * @return static
     * @throws \think\admin\Exception
     */
    public function setQueueMessage(int $total, int $count, string $message = '', int $backline = 0): Command
    {
        $this->queue->message($total, $count, $message, $backline);
        return $this;
    }
}